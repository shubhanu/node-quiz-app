import express from "express";
import http from "http";
import SocketIO from "socket.io";
import { setTimeout } from "timers";
import { userInfo } from "os";
import { MockData as ques } from "./questions";

let app = express();
let server = http.Server(app);
let io = new SocketIO(server);
let port = process.env.PORT || 8080;

let users = {};
let userSockets = {};
let onGoingMatch = {};
app.use(express.static(__dirname + "/node_modules"));
app.get("/", (req, res, next) => {
  res.sendFile(__dirname + "/index.html");
});

server.listen(8080);
let rooms = {};
// console.log("Server running at http://127.0.0.1:1337/");

io.on("connection", socket => {
  // io.sockets.emit("users_list", users);
  console.log("client connected");
  socket.on("create_user", (data, cb) => {
    if (data) {
      socket.userName = data.name;
      users = {
        ...users,
        [data.name]: {
          ...data,
          availablity: true
        }
      };
      userSockets[data.name] = socket;
    }
    cb(true);
    io.sockets.emit("game_list", users);
  });
  socket.on("get_gameList", (data, cb) => {
    cb(users);
  });
  socket.on("user_select", ({ userName, playerName }, cbb) => {
    const match_id = new Date().getTime().toString();
    console.log(userName);
    socket.match_id = match_id;
    userSockets[userName].match_id = match_id;

    socket.opponentName = userName;
    socket.userName = playerName;
    userSockets[userName].opponentName = playerName;

    socket.join(match_id);
    userSockets[userName].join(match_id);

    console.log("room joined");
    const randQues = ques
      .sort(function() {
        return 0.5 - Math.random();
      })
      .slice(0, 6);
    io.to(match_id).emit("get_ready", {
      challenger: socket.userName,
      opponent: playerName,
      matchStartTime: 5,
      quesList: randQues
    });
    onGoingMatch = {
      ...onGoingMatch,
      [match_id]: {
        _id: match_id,
        users: {
          challenger: socket.userName,
          opponent: userName
        },
        quesList: randQues,
        score: {
          [socket.userName]: {
            current_ball: 0,
            over_summary: [],
            total_score: 0,
            wickets: 0
          },
          [userName]: {
            current_ball: 0,
            over_summary: [],
            total_score: 0,
            wickets: 0
          }
        },
        results: {}
      }
    };
    console.log("timer sent");
  });

  socket.on("hit", data => {
    onGoingMatch[socket.match_id].score[socket.userName].current_ball += 1;
    onGoingMatch[socket.match_id].score[socket.userName].over_summary.push(
      data.hit
    );
    if (data.hit != "wk") {
      onGoingMatch[socket.match_id].score[socket.userName].total_score +=
        data.hit;
    } else {
      onGoingMatch[socket.match_id].score[socket.userName].wickets += 1;
    }
    console.log("entry saved as" + data.hit);
    if (
      onGoingMatch[socket.match_id].score[socket.userName].current_ball ==
        onGoingMatch[socket.match_id].score[socket.opponentName].current_ball &&
      onGoingMatch[socket.match_id].score[socket.opponentName].current_ball <
        7 &&
      onGoingMatch[socket.match_id].score[socket.userName].current_ball < 7
    ) {
      io.to(socket.match_id).emit("next_ball", {
        wickets:
          onGoingMatch[socket.match_id].score[socket.opponentName].wickets,
        runs:
          onGoingMatch[socket.match_id].score[socket.opponentName].total_score
      });
      console.log("next ball call emitted");
    }
    //  else if (
    //   onGoingMatch[socket.match_id].score[socket.opponentName].current_ball >=
    //     7 &&
    //   onGoingMatch[socket.match_id].score[socket.userName].current_ball >= 7
    // ) {
    //   io.to(socket.match_id).emit("match_over", onGoingMatch[socket.match_id]);
    // }
  });

  socket.on("disconnect", () => {
    delete userSockets[socket.userName];
  });
});
