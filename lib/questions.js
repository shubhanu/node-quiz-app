export const MockData = [
  {
    question:
      "How many times did Kolkata Knight Riders has won th IPL trophy ?",
    options: ["1", "2", "3", "4"],
    answer: "2",
    image: false
  },
  {
    question: "./src/images/viratkohli.jpg",
    options: [
      "Virat Kohli",
      "Shikhar Dhawan",
      "Yuvraj Singh",
      "Harbhajan Singh"
    ],
    answer: "Virat Kohli",
    image: true
  },
  {
    question: "Which cricketer has the best economy rate at the World Cup ?",
    options: ["Anil Kumble", "Harbhajan Singh", "Kapil Dev", "V Prasad"],
    answer: "Kapil Dev",
    image: false
  },
  {
    question:
      "Who was the wicket-keeper of the Indian Cricket Team during the World Cup 2003 tournament?",
    options: ["Parthiv Patel", "Nayan Mongia", "Rahul Dravid", "M S Dhoni"],
    answer: "Rahul Dravid",
    image: false
  },
  {
    question: "How many times did Mumbai Indians has won th IPL trophy ?",
    options: ["1", "2", "3", "4"],
    answer: "2",
    image: false
  },
  {
    question: "./src/images/msd.jpg",
    options: ["Virat Kohli", "Shikhar Dhawan", "M S Dhoni", "Harbhajan Singh"],
    answer: "M S Dhoni",
    image: true
  },
  {
    question: "Which player has maximum number of one day ducks?",
    options: [
      "Wasim Akram",
      "Romesh Kaluwitharana",
      "Inzamam-ul-Haq",
      "Sanath Jayasuriya"
    ],
    answer: "Sanath Jayasuriya",
    image: false
  },
  {
    question: "Which player has scored the fastest century?",
    options: ["Shahid Afridi", "Mark Boucher", "Brian Lara", "Corey Anderson"],
    answer: "Corey Anderson",
    image: false
  },
  {
    question: "Who is having the best one day bowling figures?",
    options: [
      "Muttiah Muralitharan",
      "Anil Kumble",
      "Chaminda Vaas",
      "Glenn McGrath"
    ],
    answer: "Chaminda Vaas",
    image: false
  },
  {
    question:
      "Who was the first player to score a One Day International century?",
    options: ["Dennis Amiss", "Derek Randall", "Don Bradman", "Mike Denniss"],
    answer: "Dennis Amiss",
    image: false
  },
  {
    question: "When did South Africa made its One Day International debut?",
    options: ["1971", "1991", "1994", "1980"],
    answer: "1991",
    image: false
  },
  {
    question: "./src/images/sunil.jpeg",
    options: [
      "Yashpal Sharma",
      "Ravi Shastri",
      "Bharath Reddy",
      "Sunil Gavaskar"
    ],
    answer: "Shane Watson",
    image: true
  },
  {
    question:
      "The world’s top cricketers were auctioned off for the DLF Indian Premier League in Mumbai on February 20, 2008. Which cricketer attracted the highest price ?",
    options: [
      "Sachin Tendulkar",
      "Mahendra Singh Dhoni",
      "Andrew Symonds",
      "Sanath Jayasuriya"
    ],
    answer: "Mahendra Singh Dhoni",
    image: false
  },
  {
    question:
      "Who won the Deodhar Trophy Cricket played in Mumbai on March 9, 2010 ?",
    options: ["North Zone", "West Zone", "East Zone", "South Zone"],
    answer: "North Zone",
    image: false
  },
  {
    question:
      "Who holds the record for the highest number of runs in Test Cricket ?",
    options: [
      "Sunil Gavaskar",
      "Geoffrey Boycott",
      "Sachin Tendulkar",
      "Gary Sobers "
    ],
    answer: "Sachin Tendulkar",
    image: false
  },
  {
    question:
      "Which country has won the ICC Twenty-20 World Cup Cricket played in May 2010 ?",
    options: ["Sri Lanka", "England", "India", "Australia"],
    answer: "England",
    image: false
  },
  {
    question: "Standard cricket bats are made of—",
    options: ["Pine wood", "Rose wood", "Teak wood", "Willow wood"],
    answer: "Willow wood",
    image: false
  },
  {
    question: "./src/images/watson.jpg",
    options: ["Glen Maxwell", "Shane Watson", "Steve Smith", "David Warner"],
    answer: "Shane Watson",
    image: true
  }
];
